import React, { Component } from 'react';
import NewTodo from '../../components/todo/new'

import { gql } from "apollo-boost";

import socketClient from '../../utils/SocketConnector';
import Items from '../../components/todo/items';

const client = socketClient()
const CHANNEL = 'infoTopic'


class Todos extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
  }

  render() {
    client
    .subscribe({
        query: gql`
    subscription{
      newTodo{
        id
        title
        description
      }
    }
    `,
      variables: { repoFullName: CHANNEL },
    }).subscribe({
      next(data) {
        window.location.reload(); 
      },
      error(err) { console.error('err', err); },
    });
    return (
      <div>
        <div>
          <NewTodo
            onCreateNewTodo={this.createNewTodo}
          />
        </div>
        <div style={{ display: 'flex', marginTop: '10px'}}>
          <Items/>
        </div>
      </div>
    );
  }
}

export default Todos;
