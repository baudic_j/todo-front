import React, { Component } from 'react';
import { gql } from "apollo-boost";

import Todo from './index'

import { Query } from "react-apollo";
const Items = () => (
  <Query
    pollInterval={0}
    query={gql`
    {
      todos{
        id
        title
        description
      }
    }
    `}
  >
    {({ loading, error, data }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <p>Error :(</p>;

        console.log('data = ', data.todos)
      return (data.todos || []).map(({ id, title, description }) => (
        <Todo
          id={id}
          title={title}
          description={description}
      />)
      );
    }}
  </Query>
);

export default Items;
