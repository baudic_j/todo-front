import React, { Component } from 'react';

import { Mutation } from "react-apollo";
import { gql } from "apollo-boost";


const ADD_TODO = gql`
  mutation AddTodo($title: String!, $description: String!) {
    addTodo(title: $title, description: $description) {
      id
      title
      description
    }
  }
`;

class newTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: ''
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({[e.target.name]: e.target.value});
  }

  handleSubmit(event) {
    const { onCreateNewTodo } = this.props;
    onCreateNewTodo(this.state)
    event.preventDefault();
  }

  render() {
    let titleInput
    let descriptionInput
    console.log('state = ', this.state)
    return (
      <Mutation mutation={ADD_TODO}>
      {(addTodo, { data }) => (
        <div>
          <form
            onSubmit={e => {
              e.preventDefault();
              addTodo({ variables: { title: titleInput.value, description: descriptionInput.value } });
              titleInput.value = "";
              descriptionInput.value = ""
            }}
          >
            <input
              ref={node => {
                titleInput = node;
              }}
            />

            <input
              ref={node => {
                descriptionInput = node;
              }}
            />
            <button type="submit">Add Todo</button>
          </form>
        </div>
      )}
    </Mutation>
      // <div>
      //   <form onChange={this.handleChange} onSubmit={this.handleSubmit}>
      //       <label>
      //         title:
      //         <input type="text" name="title" value={title} />
      //       </label>
      //       <label>
      //         description:
      //         <input type="text" name="description" value={description} />
      //       </label>
      //       <input type="submit" value="Submit" />
      //     </form>
      // </div>
    );
  }
}

export default newTodo;
