import React, { Component } from 'react';
import style from './style.css'

const Todo = ({ id, title, description }) => (
  <div className="card">
    <div className="container">
      {id}
    </div>
    <div>
      {title}
    </div>
    <div>
      {description}
    </div>
  </div>
)

export default Todo;
