import { ApolloClient, gql } from "apollo-boost";

import { WebSocketLink } from "apollo-link-ws";
import { SubscriptionClient } from "subscriptions-transport-ws";
import { InMemoryCache } from 'apollo-cache-inmemory'

const GRAPHQL_ENDPOINT = 'ws://localhost:7000/graphql';

let socketClient = null

const getSocketClient = () => {
  if (!socketClient) {
    const client = new SubscriptionClient(GRAPHQL_ENDPOINT, {
      reconnect: true
    });
    
    const link = new WebSocketLink(client);
    
    socketClient = new ApolloClient({
      link,
      cache: new InMemoryCache()
    });
  }
  return socketClient
}

export default getSocketClient