import React from 'react';
import ReactDOM from 'react-dom';
import Todos from './containers/todos';

import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";


const client = new ApolloClient({
  uri: "http://localhost:7000/graphql"
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <Todos />
  </ApolloProvider>
    , document.getElementById('root'));
